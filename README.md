# Iconos redes sociales

Una lista de las redes sociales más utilizadas

## Uso

Requiere previamente contar una librería de íconos, cualquiera puede servir, probado con fontawensome y bootstrap-icons

- Para crear un enlace a red social:

```
<a href="https://www.facebook.com/linuxitos/" target="_blank" class="btn-social btn-lg btn-behance"><i class="fab fa-behance"></i></a>
```

Descargar el proyecto y probar

![alt tag](css/1.png)

## License
For open source projects, say how it is licensed.